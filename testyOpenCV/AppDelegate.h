//
//  AppDelegate.h
//  testyOpenCV
//
//  Created by John Fred Davis on 5/6/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

