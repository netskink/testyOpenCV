//
//  main.m
//  testyOpenCV
//
//  Created by John Fred Davis on 5/6/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
