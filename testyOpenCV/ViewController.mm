//
//  ViewController.m
//  testyOpenCV
//
//  Created by John Fred Davis on 5/6/16.
//  Copyright © 2016 Netskink Computing. All rights reserved.
//
//
//  This made from this tutorial
//
// https://jkbdev.wordpress.com/2015/09/22/getting-started-with-opencv-on-ios/
//
//
//

#import "ViewController.h"
#import <opencv2/opencv.hpp>

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *theImageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClickButton:(UIButton *)sender {
    
        cv::Mat originalMat = [self cvMatFromUIImage:self.theImageView.image];
    
        cv::Mat grayMat;
        cv::cvtColor(originalMat, grayMat, CV_BGR2GRAY);
        
        // convert gray mat back to UIImage
        [self.theImageView setImage:[self UIImageFromCVMat:grayMat]];
}

- (IBAction)clickOnResetButton:(UIButton *)sender {
    
    [self.theImageView setImage:[UIImage imageNamed:@"Cthulhu.2"]]; // Cthulhu.2
}


- (cv::Mat)cvMatFromUIImage:(UIImage*)image{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;

    // From the other example. This gives an exception though.
    //cv::Mat cvMat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels

    // From the gray example which worked.
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,     // Pointer to data
                                                    cols,           // Width of bitmap
                                                    rows,           // Height of bitmap
                                                    8,              // Bits per component
                                                    cvMat.step[0],  // Bytes per row
                                                    colorSpace,     // Color space
                                                    kCGImageAlphaNoneSkipLast
                                                    | kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    return cvMat;
}

- (UIImage *)UIImageFromCVMat:(cv::Mat)cvMat{
    
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    
    CGColorSpaceRef colorspace;
    
    if (cvMat.elemSize() == 1) {
        colorspace = CGColorSpaceCreateDeviceGray();
    }else{
        colorspace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Create CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols, cvMat.rows, 8, 8 * cvMat.elemSize(), cvMat.step[0], colorspace, kCGImageAlphaNone | kCGBitmapByteOrderDefault, provider, NULL, false, kCGRenderingIntentDefault);
    
    // get uiimage from cgimage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorspace);
    return finalImage;
}


@end
